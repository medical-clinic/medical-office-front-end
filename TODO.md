##Le reste a faire pour l'API Medical Office
**️✔** Done  
**x** Todo
###General: 
* decompose appointments page to children components **x**
* change date format to dd/MM/yyyy https://github.com/sapozhnikovay/ngx-mat-datefns-date-adapter **x** 
* trim input in from **x**
* add popup to inform user with error, success,infos **x**
* use ResponseEntity **x**
* adding prescriptions  **x**
* update angular and ts versions **x**
* UserModelRest **x**

#### Controleur Payment
* list payment **️✔**
* implement accessibility **x**

#### Controleur DatabaseManagement
* verification account admin when restore database **x**
    
#### User
* fix problem token
* change password without change other user information **x**
* admin can edit roles from IHM **x**

#### Validators
* Finaliser les validateurs **x**


### Tests
* complete Unit tests **x**
* functional tests  **x**
* Integration tests **x**

### Docker
* complete Dockerlization  **x**
