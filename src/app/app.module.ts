import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule, Routes} from '@angular/router';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {TranslateModule} from '@ngx-translate/core';


import {FuseModule} from '@fuse/fuse.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule} from '@fuse/components';

import {fuseConfig} from 'app/fuse-config';

import {AppComponent} from 'app/app.component';
import {LayoutModule} from 'app/layout/layout.module';
import {DashboardModule} from "./main/dashboard/dashboard.module";
import {HttpClientInMemoryWebApiModule} from "angular-in-memory-web-api";
import {AuthenticationModule} from "./main/authentication/authentication.module";
import {PatientsModule} from "./main/patients/patients.module";
import {Error404Module} from "./main/errors/404/error-404.module";
import {Error500Module} from "./main/errors/500/error-500.module";
import {AuthGuard} from "./_shared/guards/auth.guard";
import {JwtModule} from "@auth0/angular-jwt";
import {httpInterceptorProviders} from './_shared/interceptors/interceptor_old';
import {SettingsModule} from './main/settings/settings.module';
import {SimpleNotificationsModule} from 'angular2-notifications';

import 'hammerjs';
import 'mousetrap';
import {GalleryModule} from '@ks89/angular-modal-gallery';
import {Medical_fileModule} from "./main/patients/medical-file/medical_file.module";
import {ConsultationDetailModule} from "./main/patients/medical-file/consultations/consultation-detail/consultation-detail.module";

import {AllAppointmentsComponent} from './main/appointments/all-appointments.component';
import {AllAppointmentsModule} from "./main/appointments/all-appointments.module";
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatDividerModule} from "@angular/material/divider";

import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {MAT_DATE_LOCALE} from "@angular/material/core";
import {NoAuthGuard} from "./_shared/guards/noAuth.guard";
import {KEY_TOKEN} from "./_shared/constantes";

// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


export function tokenGetter() {
    return localStorage.getItem(KEY_TOKEN);
}

const appRoutes: Routes = [

    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    // Redirect signed in user to the '/dashboards/project'
    //
    // After the user signs in, the sign in page will redirect the user to the 'signed-in-redirect'
    // path. Below is another redirection for that path to redirect the user to the desired
    // location. This is a small convenience to keep all main routes together here on this file.
    {path: 'signed-in-redirect', pathMatch: 'full', redirectTo: 'dashboard'},

    // Auth routes for authenticated users
    {
        path: 'dashboard',
        loadChildren: () => import('./main/dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [AuthGuard],
        //canActivateChild: [AuthGuard],
    },
    {
        path: 'all-appointments',
        loadChildren: () => import('./main/appointments/all-appointments.module').then(m => m.AllAppointmentsModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'patients',
        loadChildren: () => import('./main/patients/patients.module').then(m => m.PatientsModule),
        canActivate: [AuthGuard],

    },
    {
        path: 'payments',
        loadChildren: () => import('./main/payments/payments.module').then(m => m.PaymentsModule),
        canActivate: [AuthGuard],

    },
    {
        path: 'medical-file/:patientId/:fullname',
        loadChildren: () => import('./main/patients/medical-file/medical_file.module').then(m => m.Medical_fileModule)
        , canActivate: [AuthGuard],
    },
    {
        path: 'consultationDetail/:consId/:patientId/:fullname',
        loadChildren: () => import('./main/patients/medical-file/consultations/consultation-detail/consultation-detail.module').then(m => m.ConsultationDetailModule)
        , canActivate: [AuthGuard],
    },
    {
        path: 'settings',
        loadChildren: () => import('./main/settings/settings.module').then(m => m.SettingsModule)
        , canActivate: [AuthGuard],
    },
    // Auth routes for guests
    {
        path: 'auth',
        loadChildren: () => import('./main/authentication/authentication.module').then(m => m.AuthenticationModule),
        //  canLoad:[AuthGuard]
        canActivate: [NoAuthGuard],

    },


    {
        path: '**',
        loadChildren: () => import('./main/errors/404/error-404.module').then(m => m.Error404Module)
    }

];

@NgModule({
    declarations: [
        AppComponent,
        AllAppointmentsComponent,


    ],
    imports: [
        GalleryModule.forRoot(),
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),

        HttpClientInMemoryWebApiModule,
        // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
// and returns simulated server responses.
// Remove it when a real server is ready to receive requests.
        // HttpClientInMemoryWebApiModule.forRoot(
        //     FakeDbService, {dataEncapsulation: false}
        // ),
// InMemoryWebApiModule.forRoot(FakeDbService,{
//     delay :0,
//     passThruUnknownUrl:true
// }),
        // Material moment date module
        MatMomentDateModule,


        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                //  allowedDomains: ["example.com"],
                // disallowedRoutes: ["http://example.com/examplebadroute/"],
            },
        }),

        // App modules
        LayoutModule,
        /* My Modules */
        Medical_fileModule, ConsultationDetailModule, AllAppointmentsModule,
        MatCardModule, MatFormFieldModule,
        DashboardModule, PatientsModule, SettingsModule,
        AuthenticationModule,
        Error404Module, Error500Module,

        /* End My Modules */

        // NoopAnimationsModule
        /* Angular Material */
        MatInputModule, MatTableModule, MatPaginatorModule, MatDividerModule,
        MatButtonModule, MatIconModule,
        SimpleNotificationsModule.forRoot(
            {
                timeOut: 1500,
                showProgressBar: true
            }
        ),
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        AuthGuard,
        httpInterceptorProviders,
        {useClass: HashLocationStrategy, provide: LocationStrategy} // surge,
        , {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'}
    ],

})
export class AppModule {
}
