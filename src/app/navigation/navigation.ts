// import { FuseNavigation } from '@fuse/types';
//
//
// import { AuthenticationService } from "../main/authentication/login/authentication.service";
// import {Injectable} from "@angular/core";
// import {VisitorAppointmentService} from "../main/appointments/appointmentsFirstVisit/appointment-new-patient.service";
// @Injectable({
//     providedIn: 'root'
// })
// class Nav{
//   public navigation: FuseNavigation[] ;
//     constructor(public authService: AuthenticationService){
//
//         this.navigation= [
//             {
//                 id       : 'applications',
//                 title    : 'Applications',
//                 translate: 'NAV.APPLICATIONS',
//                 type     : 'group',
//                 children : [
//                     {
//                         id       : 'dashboard',
//                         title    : 'Dashboard',
//                         translate: 'NAV.DASHBOARD',
//                         type     : 'item',
//                         icon     : 'dashboard',
//                         url      : '/dashboard'
//                     },
//                     {
//                         id :'appointments',
//                         title:'Appointments',
//                         translate:'NAV.Appointments',
//                         type :'item',
//                         icon : 'calendar_today',
//                         url: '/appointments'
//                     }
//                     ,
//                     {
//                         id :'patients',
//                         title:'Patients',
//                         translate:'NAV.Patients',
//                         type :'item',
//                         icon : 'accessible',
//                         url: '/patients'
//                     },
//                     {
//                         id       : 'sample',
//                         title    : 'Sample',
//                         translate: 'NAV.SAMPLE.TITLE',
//                         type     : 'item',
//                         icon     : 'email',
//                         url      : '/sample',
//                         badge    : {
//                             title    : '25',
//                             translate: 'NAV.SAMPLE.BADGE',
//                             bg       : '#F44336',
//                             fg       : '#FFFFFF'
//                         }
//                     },
//                     {
//                         id :'settings',
//                         title:'Settings',
//                         translate:'NAV.Settings',
//                         type :'item',
//                         icon : 'settings',
//                         url: '/settings',
//                         hidden:this.authService.isAdmin(),
//                     },
//
//                 ]
//             }
//         ];
//     }
// }

import {FuseNavigation} from "../../@fuse/types";

// export
const navigation: FuseNavigation[] = [   // unused
    {
        id: 'applications',
        title: 'Applications',
        translate: 'NAV.APPLICATIONS',
        type: 'group',
        icon: 'apps',
        children: [
            {
                id: 'dashboard',
                title: 'Dashboard',
                translate: 'NAV.DASHBOARD',
                type: 'item',
                icon: 'dashboard',
                url: '/dashboard'
            },
            {
                id: 'appointments',
                title: 'Appointments',
                translate: 'NAV.Appointments',
                type: 'item',
                icon: 'calendar_today',
                url: '/all-appointments',
                // badge    : {
                //     title    : '25',
                //     translate: 'NAV.SAMPLE.BADGE',
                //     bg       : '#F44336',
                //     fg       : '#FFFFFF'
                // }
            }
            ,
            {
                id: 'patients',
                title: 'Patients',
                translate: 'NAV.Patients',
                type: 'item',
                icon: 'accessible',
                url: '/patients'
            },
            {
                id: 'medications',
                title: 'Medications',
                translate: 'NAV.Medications',
                type: 'item',
                icon: 'medications',
                url: '/medications'
            },
            {
                id: 'payments',
                title: 'Payments',
                translate: 'NAV.Payments',
                type: 'item',
                icon: 'payments',
                url: '/payments'
            },


            {
                id: 'settings',
                title: 'Settings',
                translate: 'NAV.Settings',
                type: 'item',
                icon: 'settings',
                url: '/settings',
                // hidden:true
            },
        ]
    },
    {
        id: 'help',
        title: 'Help',
        translate: 'NAV.HELP',
        type: 'group',
        icon: 'help',
        children: [
            {
                id: 'support',
                title: 'Support',
                translate: 'NAV.SUPPORT',
                type: 'item',
                icon: 'support',
                url: '/support',

                badge: {
                    bg: '#F44336',
                    fg: '#FFFFFF'
                },

            },
        ]
    }
];

import {Injectable} from '@angular/core';
import {AuthenticationService} from "../main/authentication/login/authentication.service";
import {AppComponent} from "../app.component";


@Injectable({
    providedIn: 'root'
})
export class NavigationService {
    private navigation: FuseNavigation[];

    constructor(
        //private  authService: AuthenticationService,
    ) {
        this.updateNavigationSideMenu()
    }

    public updateNavigationSideMenu() {
        this.navigation =
            [
                {
                    id: 'applications',
                    title: 'Applications',
                    translate: 'NAV.APPLICATIONS',
                    type: 'group',
                    icon: 'apps',
                    children: [
                        {
                            id: 'dashboard',
                            title: 'Dashboard',
                            translate: 'NAV.DASHBOARD',
                            type: 'item',
                            icon: 'dashboard',
                            url: '/dashboard'
                        },
                        {
                            id: 'appointments',
                            title: 'Appointments',
                            translate: 'NAV.Appointments',
                            type: 'item',
                            icon: 'calendar_today',
                            url: '/all-appointments',
                            // badge    : {
                            //     title    : '25',
                            //     translate: 'NAV.SAMPLE.BADGE',
                            //     bg       : '#F44336',
                            //     fg       : '#FFFFFF'
                            // }
                        }
                        ,
                        {
                            id: 'patients',
                            title: 'Patients',
                            translate: 'NAV.Patients',
                            type: 'item',
                            icon: 'accessible',
                            url: '/patients'
                        },  {
                            id: 'payments',
                            title: 'Payments',
                            translate: 'NAV.Payments',
                            type: 'item',
                            icon: 'attach_money',
                            url: '/payments'
                        },

                        {
                            id: 'settings',
                            title: 'Settings',
                            translate: 'NAV.Settings',
                            icon: 'settings',
                            type: 'collapsable',
                            children: [
                                {
                                    id: 'users',
                                    title: 'Users',
                                    translate: 'NAV.USERS',
                                    type: 'item',
                                    icon: 'person',
                                    url: '/settings/users',
                                    //   hidden: this.authService.getRole() != 'ADMIN'
                                },
                                {
                                    id: 'medications',
                                    title: 'Medications',
                                    translate: 'NAV.Medications',
                                    type: 'item',
                                    url: '/settings/medications',
                                    icon: 'assignment',
                                },
                               /* {
                                    id: 'medicalTreatments',
                                    title: 'Medical treatments',
                                    translate: 'NAV.medicalTreatments',
                                    type: 'item',
                                    icon: 'assignment',
                                    url: '/settings/medicalTreatments',
                                    //   hidden: this.authService.getRole() != 'ADMIN'
                                },*/
                                {id: 'infos',
                                    title: 'Infos',
                                    translate: 'NAV.Infos',
                                    type: 'item',
                                    icon: 'info',
                                    url: '/settings/infos',
                                    //   hidden: this.authService.getRole() != 'ADMIN'


                                },
                                {
                                    id: 'database',
                                    title: 'Database',
                                    translate: 'NAV.Database',
                                    type: 'item',
                                    icon: 'backup',
                                    url: '/settings/databaseManagement',
                                    //   hidden: this.authService.getRole() != 'ADMIN'
                                },
                            ]
                        },


                    ]
                }
            ];
    }

    public getNavigation(): FuseNavigation[] {

        /*  console.log("this.authService.isAdmin()", this.authService.isAdmin())
          console.log("role", this.authService.getRole())
          console.log("roles", this.authService.roles)*/

        return this.navigation;
    }
}

