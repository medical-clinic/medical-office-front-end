export class PageDto<T> {
    totalElements: number;
    content: Array<T>;
    number: number; // current page
    size: number; // page size
}

export interface AppDataState<T>{
    dataState?: DataState,
    data?: PageDto<T>,
    errorMessage?:string
}
export enum DataState {
    LOADING,LOADED,ERROR
}