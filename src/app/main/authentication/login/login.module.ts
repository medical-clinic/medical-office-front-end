import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {LoginComponent} from "./login.component";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {FuseSharedModule} from "../../../../@fuse/shared.module";
import { AuthGuard } from 'app/_shared/guards/auth.guard';

const routes=[
    {
        path  : 'auth/login',
        component: LoginComponent,
        //canLoad:[AuthGuard]
    }
]
// export function tokenGetter() {
//     return localStorage.getItem("token");
// }
@NgModule({
  declarations: [
      LoginComponent
  ],
  imports: [
    CommonModule,
      RouterModule.forChild(routes),

      MatButtonModule,
      MatCheckboxModule,
      MatFormFieldModule,
      MatIconModule,
      MatInputModule,

      FuseSharedModule,

      // JwtModule.forRoot({
      //     config: {
      //         tokenGetter: tokenGetter,
      //       //  allowedDomains: ["example.com"],
      //        // disallowedRoutes: ["http://example.com/examplebadroute/"],
      //     },
      // }),

  ]
})
export class LoginModule { }
