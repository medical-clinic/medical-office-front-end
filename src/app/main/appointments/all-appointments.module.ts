import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {VisitorAppointmentComponent} from "./visitorsAppointments/visitor-appointment.component";
import {AuthGuard} from "../../_shared/guards/auth.guard";
import {VisitorAppointmentFormComponent} from "./visitorsAppointments/visitor-appointment-form/visitor-appointment-form.component";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatDividerModule} from "@angular/material/divider";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatCardModule} from "@angular/material/card";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatMenuModule} from "@angular/material/menu";
import {MatToolbarModule} from "@angular/material/toolbar";
import {ReactiveFormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";
import {MatTooltipModule} from "@angular/material/tooltip";
import {FuseSharedModule} from "../../../@fuse/shared.module";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatSelectModule} from "@angular/material/select";
import {AllAppointmentsComponent} from "./all-appointments.component";
import {PatientsAppointmentsComponent} from './patientsAppointments/patients-appointments.component';


const routes: Routes = [
    {
        path: '',
        component: AllAppointmentsComponent,
        canActivate: [AuthGuard]

    }
]
@NgModule({
    declarations: [
        VisitorAppointmentComponent,
        VisitorAppointmentFormComponent,
        PatientsAppointmentsComponent,
    ],
    exports: [
        VisitorAppointmentComponent,
        PatientsAppointmentsComponent
    ],
    imports: [
        CommonModule,

        RouterModule.forChild(routes),

        MatPaginatorModule,
        // MatTableDataSource,
        MatSidenavModule,
        MatDividerModule,
        FlexLayoutModule,
        MatCardModule,
        MatPaginatorModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatMenuModule,
        MatToolbarModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatTooltipModule,
        FuseSharedModule,
        MatDatepickerModule,
        MatSelectModule
    ]
})
export class AllAppointmentsModule { }
