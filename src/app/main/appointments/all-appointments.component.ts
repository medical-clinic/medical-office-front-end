import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {Router} from "@angular/router";
import {UnionAppointmentsService} from "./union-appointments.service";

@Component({
    selector: 'app-all-appointments',
    templateUrl: './all-appointments.component.html',
    styleUrls: ['./all-appointments.component.scss']
})
export class AllAppointmentsComponent implements OnInit {

    private appointments_list: Array<AppointmentsUnion> = new Array<AppointmentsUnion>();
    dataSource = new MatTableDataSource<AppointmentsUnion>(this.appointments_list);

    data = Object.assign(this.appointments_list);

    lengthTable: number;

    @ViewChild(MatPaginator, {static: true})
    paginator: MatPaginator;

    constructor(private unionService: UnionAppointmentsService

        ,private router: Router) {
    }

    public getDisplayedColumns(): string[] {

        return ['firstName', 'lastName', 'time','status']

    }

    ngOnInit() {
        // array1.forEach(element => console.log(element));
        this.initAppointments();
        //   this.intAppointment_with_promise();
    }

    initAppointments() {
        this.dataSource.paginator = this.paginator;
        this.unionService.getUnionAppointments().subscribe(
            result => {
                console.log(result)
                // this.lengthTable = result.length;
                result.forEach(element => this.appointments_list.push(element));
                this.dataSource._updateChangeSubscription();

            }, error => {
                console.error(error);
               // this.router.navigateByUrl('/auth/login'); // i must know status code == token not valid
            }
        );
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }


}

class AppointmentsUnion {
    firstName: string;
    lastName: string;
    phone: string;
    // date: string;
    time: string;
    status: string;

}