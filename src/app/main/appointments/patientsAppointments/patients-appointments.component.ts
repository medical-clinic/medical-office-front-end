import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from "../../../../@fuse/animations";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {FuseConfirmDialogComponent} from "../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {AuthenticationService} from "../../authentication/login/authentication.service";
import {Router} from "@angular/router";
import {PatientAppointmentService} from "../../patients/medical-file/patient-appointments/patient-appointment.service";
import {ResponseModel} from "./response.model";
import {convertDate} from "app/_shared/helpers"

@Component({
  selector: 'patient-appointments',
  templateUrl: './patients-appointments.component.html',
  styleUrls: ['./patients-appointments.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class PatientsAppointmentsComponent implements OnInit {

    private appointments_list: Array<ResponseModel> = new Array<ResponseModel>();
    dataSource = new MatTableDataSource<ResponseModel>(this.appointments_list);

    data = Object.assign(this.appointments_list);

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MatPaginator, {static: true})
    paginator: MatPaginator;
    pageEvent: PageEvent;
    pageIndex: number;
    pageSize: number;
    length: number;
    date: string=null;

    constructor(private appointmentOldPatientService: PatientAppointmentService, public authService:AuthenticationService,
                public dialog: MatDialog, private _matDialog: MatDialog
        , private router: Router) {
    }

    public getDisplayedColumns(): string[] {
        // if (this.authService.isSecretary()) {
            return ['firstName', 'lastName', 'date', 'time', 'Actions']
        // }else {
        //     return ['firstName', 'lastName', 'date', 'time']
        // }

    }
    ngOnInit() {
        this.getServerData(null)
        // this.initAppointments();
        
    }
    public getServerData(event?: PageEvent) {
        this.pageEvent=event;
        let pi = event != null ? event.pageIndex : 0;
        let ps = event != null ? event.pageSize : 10;
        this.appointmentOldPatientService.getAllPatientsAppointments(pi, ps,this.date).subscribe(

            response => {
                console.log("appointments Patients",response);
                
                // if (response.error) {
                //     // handle error
                // } else {
                    this.appointments_list = new Array<ResponseModel>()

                    response.content.forEach(element => this.appointments_list.push(element));
                    this.dataSource.data = this.appointments_list
                    

                    this.dataSource._updateChangeSubscription();
                    this.pageIndex = response.number;
                    
                    this.length = response.totalElements;


                    this.pageSize = response.size;
               // }
            },
            error => {
                // handle error
            }
        );
        return event;
    }
    search(date) {
        this.date= convertDate(date)
        this.getServerData(null)
        
        // this.keyword=this.keyword.trim();
        // this.getServerData(this.pageEvent);
    }
    initAppointments() {

        /*
        this.dataSource.paginator = this.paginator;
        this.appointmentOldPatientService.getOldAppointmentsOfAllPatients().subscribe(
            result => {
                console.log(result)
                // this.lengthTable = result.length;
                result.forEach(element => this.appointments_list.push(element));
                this.dataSource._updateChangeSubscription();

            }, error => {
                console.error(error);
              //  this.router.navigateByUrl('/auth/login'); // i must know status code == token not valid
            }
        );
        */ 
    }
    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }






    /**
     * Edit contact
     *
     * @param element
     */
    openMedicalFile(element) {


        this.router.navigate(['/medical-file',element.patient_id,`${element.first_name} ${element.last_name}`]);


    }
}
