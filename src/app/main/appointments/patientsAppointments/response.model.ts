export class ResponseModel{
    patient_id: number;
    first_name: string;
    last_name: string;
    date: string
    time:string
}