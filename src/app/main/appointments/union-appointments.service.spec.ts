import {TestBed} from '@angular/core/testing';

import {UnionAppointmentsService} from './union-appointments.service';

describe('UnionAppointmentsService', () => {
  let service: UnionAppointmentsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UnionAppointmentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
