import {Injectable} from '@angular/core';
import {API_URL} from "app/_shared/config/api.url.config";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UnionAppointmentsService {

    private urlResource: string = API_URL.api_union_appointments;

    constructor(private http: HttpClient) {
    }



    getUnionAppointments() : Observable<any[]> {
        return this.http.get<any[]>(this.urlResource)
    }
}
