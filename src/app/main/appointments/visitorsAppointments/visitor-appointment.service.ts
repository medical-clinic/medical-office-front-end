import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {VisitorAppointmentDto} from "./visitor-appointment.model";
import {AuthenticationService} from "../../authentication/login/authentication.service";
import {API_URL} from "app/_shared/config/api.url.config";
import {PageDto} from 'app/_shared/_models/page.model';

@Injectable({
    providedIn: 'root'
})
export class VisitorAppointmentService {
    private url: string = API_URL.api_visitor_appointments;
    constructor(private http: HttpClient, public authService: AuthenticationService) {
    }

    deleteSelectedAppointments(ids: number[]): Observable<VisitorAppointmentDto> {
        // const token = this.authService.getToken();
        // if (token) {
        return this.http.post<VisitorAppointmentDto>(this.url + `/deleteMultiRows`, ids
            //,{headers: new HttpHeaders({'Authorization': token})}
        );
        // } else {
        //     this.authService.logout();
        // }
    }



    /**
     * Get appointments
     *
     * @returns {Observable<any>}
     */
    getAppointments(pageIndex: number, pageSize: number,
        keyword: string): Observable<PageDto<VisitorAppointmentDto>> {
        return this.http.get<PageDto<VisitorAppointmentDto>>(this.url,
            {
                params: new HttpParams()
                    .set('page', pageIndex.toString())
                    .set('limit', pageSize.toString())
                    .set('keyword', keyword)

            })

    }



    //  url = 'http://localhost:8084/appointments';

    addAppointment(appointment: VisitorAppointmentDto) {
        //appointment.dateAppointment=appointment.dateAppointment.substring(1, 4);
        // this.http.post('api/appointments', appointment)


       

        // console.log("app", appointment);
        // const token = this.authService.getToken();
        // if (token) {
        return this.http.post(this.url, appointment,
            // {headers: new HttpHeaders({'Authorization': token})}
        )

        // } else {
        //     this.authService.logout();
        // }


    }

    deleteAppointment(id: number): Observable<VisitorAppointmentDto> {
        // const token = this.authService.getToken();
        // if (token) {
        return this.http.delete<VisitorAppointmentDto>(this.url + `/${id}`,
            // {headers: new HttpHeaders({'Authorization': token})}
        );
        // } else {
        //     this.authService.logout();
        // }
    }

    updateAppointment(appointment: VisitorAppointmentDto) {
        // const token = this.authService.getToken();
        // if (token) {
        return this.http.put<VisitorAppointmentDto>(this.url + `/${appointment.id}`, appointment,
            // {headers: new HttpHeaders({'Authorization': token})}
        )
        // } else {
        //     this.authService.logout();
        // }
    }

}
