import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StatisticResponse} from "./statisticResponse.model";
import {API_URL} from "app/_shared/config/api.url.config";

@Injectable({
    providedIn: 'root'
})
export class DashboardService {


    private url_statistic: string = API_URL.api_statistics;
    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
    }
    getStatistics(): Observable<StatisticResponse> {
        return this._httpClient.get<StatisticResponse>(this.url_statistic);
    }


}

