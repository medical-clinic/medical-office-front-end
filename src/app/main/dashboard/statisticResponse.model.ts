export class StatisticResponse {
    appointmentsToday: number;
    newPatients: number;
    malePercentage: number;
    femalePercentage: number;
    dataConsultation: [
        {
            year: number,
            data: Array<number>
        }
    ];

}
