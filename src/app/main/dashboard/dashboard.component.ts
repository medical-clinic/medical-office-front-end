import {Component, OnInit, ViewEncapsulation} from '@angular/core';

import {fuseAnimations} from '@fuse/animations';

import {DashboardService} from './dashboard.service';
import {StatisticResponse} from "./statisticResponse.model";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class DashboardComponent implements OnInit {

    public statistics: StatisticResponse;
    widgets: any;

    public currentYear: number=new Date().getFullYear()
    public previousYear: number= this.currentYear-1;
    widgetChartConsSelectedYear = this.currentYear;
    

    /**
     * Constructor
     *
     * @param {DashboardService} dashboardService
     */
    constructor(
        private dashboardService: DashboardService
    ) {
        // Register the custom chart.js plugin
        this._registerCustomChartJSPlugin();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    dataCons;
    /**
     * On init
     */
    ngOnInit(): void {
        // Get the widgets from the service
        // this.widgets=this.dashboardService.myWidgets;
        this.dashboardService.getStatistics()
            .subscribe(
                res => {
                   

                    this.statistics = res;
                    console.log('statistics',this.statistics)
                    let dataConsultation = this.statistics.dataConsultation;
                    let datasets = {};

                    for (let i = 0; i < dataConsultation.length; i++) {

                        
                        datasets[dataConsultation[i].year] =[
                            {
                                label: 'Consultations',
                                data: dataConsultation[i].data,
                                fill: 'start'
    
                            }
                        ] 

                    }
                     //datasets = this.dashboardService.getFakeConsDatasets()


                    this.dataCons = datasets[this.widgetChartConsSelectedYear];
                   
                    console.log(datasets)
                    this.widgets = {
                        widgetChartCons: {
                            chartType: 'line',
                            datasets: datasets,

                            labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
                            colors: [
                                {
                                    borderColor: '#42a5f5',
                                    backgroundColor: '#42a5f5',
                                    pointBackgroundColor: '#1e88e5',
                                    pointHoverBackgroundColor: '#1e88e5',
                                    pointBorderColor: '#ffffff',
                                    pointHoverBorderColor: '#ffffff'
                                }
                            ],
                            options: {
                                spanGaps: false,
                                legend: {
                                    display: false
                                },
                                maintainAspectRatio: false,
                                layout: {
                                    padding: {
                                        top: 32,
                                        left: 32,
                                        right: 32
                                    }
                                },
                                elements: {
                                    point: {
                                        radius: 4,
                                        borderWidth: 2,
                                        hoverRadius: 4,
                                        hoverBorderWidth: 2
                                    },
                                    line: {
                                        tension: 0
                                    }
                                },
                                scales: {
                                    xAxes: [
                                        {
                                            gridLines: {
                                                display: false,
                                                drawBorder: false,
                                                tickMarkLength: 18
                                            },
                                            ticks: {
                                                fontColor: '#fff'
                                            }
                                        }
                                    ],
                                    yAxes: [
                                        {
                                            display: false,
                                            ticks: {
                                               
                                                // callback: function(value, index, values) {
                                                //     return value / 1e6 + 'M';
                                                // }
                                                // min: 1.5,
                                                // max: 5,
                                                // stepSize: 0.5,

                                            }
                                        }
                                    ]
                                },
                                plugins: {
                                    filler: {
                                        propagate: false
                                    },
                                    xLabelsOnTop: {
                                        active: true
                                    }
                                }
                            }
                        },
                        widgetChartGender: {
                            chartType: 'pipe',
                            scheme: {
                                domain: ['#5AA454', '#A10A28']
                            },
                            results: [
                                {
                                    name: 'Males',
                                    value: this.statistics.malePercentage
                                },
                                {
                                    name: 'Females',
                                    value: this.statistics.femalePercentage
                                }
                            ]
                        }
                    };
                    
                },
                error => console.error(error)
            );

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Register a custom plugin
     */
    private _registerCustomChartJSPlugin(): void {
        (window as any).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                ) {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index): any {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (window as any).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }


    public onChangeYear(selectedYear:number){
        this.widgetChartConsSelectedYear=selectedYear;
        this.dataCons=this.widgets.widgetChartCons.datasets[this.widgetChartConsSelectedYear]

    }
}
