import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';

import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UserRequest} from "../user.model";
import {of, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class UserFormComponent implements OnInit, OnDestroy {
    action: string;
    user: UserRequest;
    userForm: FormGroup;
    dialogTitle: string;
    // optionSelected: string;
    // options=[
    //     {value: 'DOCTOR', viewValue: 'DOCTOR'},
    //     {value: 'SECRETARY', viewValue: 'SECRETARY'},
    //     {value: 'ADMIN', viewValue: 'ADMIN'}
    // ];
    roles = [];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<AppointmentFormComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<UserFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit User';
            this.user = _data.user;
            this.userForm = this.createEditUserForm();

        } else {
            this.dialogTitle = 'New User';
            this.user = new UserRequest({});
            this.userForm = this.createNewUserForm();
        }
    }

    /**
     * Create user form
     *
     * @returns {FormGroup}
     */
    createNewUserForm(): FormGroup {
        let myformGroup: FormGroup = this._formBuilder.group({
            id: [this.user.id],
            username: [this.user.username, [Validators.required, Validators.minLength(4)]],
            password: [this.user.password, [Validators.required, Validators.minLength(8)]],
            confirmPassword: [this.user.confirmPassword, [Validators.required,
                confirmPasswordValidator
            ]
            ],
            active: [this.user.active],
            // roles: [[{roleName:this.optionSelected}]]
            //  roles: [this.user.roles]
            roleName: [this.user.roleName]
        });
        // async orders
        of(this.getRoles()).subscribe(roles => {
            this.roles = roles;
            //   myformGroup.controls.roleName.patchValue(this.roles[0].roleName);
            myformGroup.controls.roleName.patchValue(this.user.roleName);
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        myformGroup.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                myformGroup.get('confirmPassword').updateValueAndValidity();
            });

        return myformGroup;
    }


    createEditUserForm(): FormGroup {
        let myformGroup: FormGroup = this._formBuilder.group({
            id: [this.user.id],
            username: [this.user.username, [Validators.required, Validators.minLength(4)]],
            active: [this.user.active],
            // roles: [[{roleName:this.optionSelected}]]
            //  roles: [this.user.roles]
            roleName: [this.user.roleName]
        });
        // async orders
        of(this.getRoles()).subscribe(roles => {
            this.roles = roles;
            //   myformGroup.controls.roleName.patchValue(this.roles[0].roleName);
            myformGroup.controls.roleName.patchValue(this.user.roleName);
        });

        return myformGroup;
    }

    getRoles() {
        return [
            { roleName: 'DOCTOR' },
            { roleName: 'NURSE' },
            { roleName: 'ADMIN' },

        ];
    }

    ngOnInit(): void {

    }
    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('confirmPassword');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { passwordsNotMatching: true };
};
