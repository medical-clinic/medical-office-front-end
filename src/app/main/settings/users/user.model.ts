export class UserRequest {
    id: number
    username: string
    active: boolean
    password: string
    confirmPassword: string
    roleName: string

    /**
     * Constructor
     *
     * @param user
     */
    constructor(user) {
        {
            this.id = user.id || null;
            this.username = user.username || '';
            this.password = user.password || '';
            this.confirmPassword = user.confirmPassword || '';
            this.active = user.active || true;
            this.roleName = user.roleName || 'NURSE';

        }
    }
}
export class UserResponse {
    id: number
    username: string
    active: boolean
    roleName: string

    /**
     * Constructor
     *
     * @param user
     */
    constructor(user) {
        {
            // this.id = user.id || FuseUtils.generateGUID();
            this.username = user.username || '';
            this.active = user.active || true;
            this.roleName = user.roleName || '';

        }
    }
}

// export class Role {
//     id: number;
//     nameRole: string;
// }

