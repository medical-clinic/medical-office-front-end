import {Component, OnInit, ViewChild} from '@angular/core';
import {UserResponse} from "./user.model";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {FuseConfirmDialogComponent} from "../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {UserFormComponent} from "./user-form/user-form.component";
import {FormGroup} from "@angular/forms";
import {UserService} from "./user.service";
import {PasswordUserFormComponent} from "./password-user-form/password-user-form.component";

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

    // Username	isActive	Roles	Action
    displayedColumns: string[] = ['id', 'username', 'roles', 'active', 'Actions'];
    private users_list: Array<UserResponse> = new Array<UserResponse>();
    dataSource = new MatTableDataSource<UserResponse>(this.users_list);
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;
    pageEvent: PageEvent;
    pageIndex: number;
    pageSize: number;
    length: number;
    public keyword: string = ''

    constructor(private userService: UserService
        , public dialog: MatDialog,
        private _matDialog: MatDialog,) {
    }


    ngOnInit() {
        this.getServerData(null)
    }

    public getServerData(event?: PageEvent) {
        this.pageEvent = event;
        let pi = event != null ? event.pageIndex : 0;
        let ps = event != null ? event.pageSize : 5;
        this.userService.getUsers(pi, ps, this.keyword).subscribe(

            response => {
                console.log(response);

                // if (response.error) {
                //     // handle error
                // } else {
                this.users_list = new Array<UserResponse>()

                response.content.forEach(element => this.users_list.push(element));
                this.dataSource.data = this.users_list


                this.dataSource._updateChangeSubscription();
                this.pageIndex = response.number;
                this.length = response.totalElements;
                this.pageSize = response.size;
                // }
            },
            error => {
                // handle error
            }
        );
        return event;
    }
    search() {
        this.keyword = this.keyword.trim();
        this.getServerData(this.pageEvent);

    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    addUser() {

        this.dialogRef = this._matDialog.open(UserFormComponent, {
            panelClass: 'user-form-dialog',
            data: {
                action: 'new'
            }
        });
        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                console.log(response.getRawValue())

                //
                this.userService.addUser(response.getRawValue())
                    .subscribe(
                        response => {
                            console.log(response);
                            this.getServerData(this.pageEvent);
                        },
                        err => console.error(err)
                    );


            });
    }

    deleteUser(id: number) {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log('id', id);


                this.userService.deleteUser(id)
                    .subscribe(result => {
                        this.getServerData(this.pageEvent);
                        console.log(result);
                    }, err => {
                        console.error(err);
                    }
                    );
            }
            this.confirmDialogRef = null;
        });
    }

    editUser(element: UserResponse) {
        
        this.dialogRef = this._matDialog.open(UserFormComponent, {
            panelClass: 'user-form-dialog',
            data: {
                user: element,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.userService.updateUser(formData.getRawValue())
                            .subscribe((res) => {
                                console.log(res);
                                this.getServerData(this.pageEvent);

                            }, (err) => console.error(err)
                            );

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteUser(formData.getRawValue().id);

                        break;
                }
            });
    }

    changePasswordUser(element) {
        this.dialogRef = this._matDialog.open(PasswordUserFormComponent, {
            panelClass: 'password-user-form-dialog',
            data: {
                user: element,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.userService.changePassword(formData.getRawValue())
                            .subscribe((res) => {
                                    console.log(res);
                                    this.getServerData(this.pageEvent);

                                }, (err) => console.error(err)
                            );

                        break;

                }
            });
    }
}
