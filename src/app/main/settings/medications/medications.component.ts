import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from "../../../../@fuse/animations";
import {MedicationsService} from "./medications.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {FuseConfirmDialogComponent} from "../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {AuthenticationService} from "../../authentication/login/authentication.service";
import {Router} from "@angular/router";
import {FormGroup} from "@angular/forms";
import {SelectionModel} from "@angular/cdk/collections";
import {MedicationDto} from "./medication.model";
import {MedicationFormComponent} from "./medication-form/medication-form.component";
import {NotificationsService, NotificationType} from "angular2-notifications";

@Component({
    selector: 'app-medications',
    templateUrl: './medications.component.html',
    styleUrls: ['./medications.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class MedicationsComponent implements OnInit {

    private medication_list: Array<MedicationDto> = new Array<MedicationDto>();
    dataSource = new MatTableDataSource<MedicationDto>(this.medication_list);

    selection = new SelectionModel<MedicationDto>(true, []);
    data = Object.assign(this.medication_list);

    @ViewChild(MatPaginator)  // {static: true}
    paginator: MatPaginator;
    pageEvent: PageEvent;
    pageIndex: number;
    pageSize: number;
    length: number;

    keyword: string = '';

    constructor(private medicationsService: MedicationsService, public authService: AuthenticationService,
                public dialog: MatDialog, private _matDialog: MatDialog
        , private router: Router,
                private _notifications: NotificationsService,) {
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    /**
     * Delete selected contacts
     */
    removeSelectedRows(): void {
        let ids: number[] = [];

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete all selected contacts?';
        this.confirmDialogRef.afterClosed()
            .subscribe(result => {
                if (result) {
                    // this.selection.hasValue()

                    this.selection.selected.forEach(item => {
                        console.log(item.id);
                        ids.push(item.id);
                        //   let index: number = this.data.findIndex(d => d === item);
                        // console.log(this.data.findIndex(d => d === item));
                        //   this.data.splice(index, 1)
                        //   this.dataSource = new MatTableDataSource<Announcement>(this.data);
                    });
                    // this.selection = new SelectionModel<Announcement>(true, []);
                    this.medicationsService.deleteSelected(ids)
                        .subscribe(
                            result => {
                                console.log(result);
                                this.getServerData(this.pageEvent);
                                this.showNotificationSuccess();
                            },
                            error => {
                                this.showNotificationError();
                                console.error(error)
                            }
                        );
                }
                this.confirmDialogRef = null;
            });

        //////////////

        // this.selection.selected.forEach(item => {
        //   console.log(item.id);
        //   let index: number = this.data.findIndex(d => d === item);
        //   // console.log(this.data.findIndex(d => d === item));
        //   this.data.splice(index, 1)
        //   this.dataSource = new MatTableDataSource<Announcement>(this.data);
        // });
        // this.selection = new SelectionModel<Announcement>(true, []);

        //////////////


    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: MedicationDto): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }

    public getDisplayedColumns(): string[] {
        return ['select', 'name', 'Actions']
        /* if (this.authService.isSecretary()) {
             return ['select', 'name', 'Actions']
         } else {
             return ['name']
         }*/

    }

    ngOnInit() {
        this.getServerData(null)
    }
    search() {
        this.keyword = this.keyword.trim();
        this.getServerData(this.pageEvent);

    }


    public getServerData(event?: PageEvent) {
        this.pageEvent = event;
        let pi = event != null ? event.pageIndex : 0;
        let ps = event != null ? event.pageSize : 10;
        this.medicationsService.getAll(pi, ps, this.keyword).subscribe(
            response => {
                console.log(response);

                // if (response.error) {
                //     // handle error
                // } 
                // else {
                this.medication_list = new Array<MedicationDto>()

                response.content.forEach(element => this.medication_list.push(element));
                this.dataSource.data = this.medication_list


                this.dataSource._updateChangeSubscription();


                this.pageIndex = response.number;

                this.length = response.totalElements;


                this.pageSize = response.size;
                // }
            },
            error => {
                // handle error
            }
        );
        return event;
    }


    dialogRef: any;

    add() {
        this.dialogRef = this._matDialog.open(MedicationFormComponent, {
            panelClass: 'medication-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }

                this.medicationsService.add(response.getRawValue())
                    .subscribe(response => {
                        //  this.getAnnouncements();
                        // resolve(response);
                        console.log(response);
                        this.getServerData(this.pageEvent);
                        this.showNotificationSuccess();
                    }, error => this.showNotificationError());
            });
    }

    /**
     * Delete Announcement
     */
    delete(id: number) {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log('id', id);


                this.medicationsService.delete(id)
                    .subscribe(result => {
                            console.log(result);
                            this.getServerData(this.pageEvent);
                            this.showNotificationSuccess();
                        }, err => {
                            console.error(err);
                            this.showNotificationError();
                        }
                    );
            }
            this.confirmDialogRef = null;
        });

    }

    /**
     * Edit contact
     *
     * @param element
     */
    edit(element: MedicationDto) {


        this.dialogRef = this._matDialog.open(MedicationFormComponent, {
            panelClass: 'medication-form-dialog',
            data: {
                medication: element,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':

                        this.medicationsService.update(formData.getRawValue())
                            .subscribe((res) => {
                                    console.log(res);
                                    this.getServerData(this.pageEvent);
                                    this.showNotificationSuccess();
                                }, err => {
                                    this.showNotificationError()
                                    console.error(err)
                                }
                            );

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.delete(formData.getRawValue().id);

                        break;
                }
            });

    }

    private showNotificationSuccess(): void {
        this._notifications.create('Success', 'content', NotificationType.Success)
    }

    private showNotificationError(): void {
        this._notifications.create('Error', 'content', NotificationType.Error,
            {
                timeOut: 3000,
            })
    }
}
