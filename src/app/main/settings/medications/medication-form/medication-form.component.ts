import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MedicationDto} from "../medication.model";

@Component({
  selector: 'app-announcement-form',
  templateUrl: './medication-form.component.html',
  styleUrls: ['./medication-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MedicationFormComponent implements OnInit {

    action: string;
    medication: MedicationDto;
    formGroup: FormGroup;
    dialogTitle: string;

    /**
     * Constructor
     *
     * @param {MatDialogRef<MedicationFormComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<MedicationFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Medication';
            this.medication = _data.medication;

        }
        else {
            this.dialogTitle = 'New Medications';
            this.medication = new MedicationDto({});
        }

        this.formGroup = this.createForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.medication.id],
            name: [this.medication.name],
        });
    }

    ngOnInit(): void {
    }

}
