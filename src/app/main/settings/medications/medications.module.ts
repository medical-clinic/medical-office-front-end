import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MedicationsComponent} from "./medications.component";
import {Route, RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {FuseSharedModule} from "../../../../@fuse/shared.module";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatDividerModule} from "@angular/material/divider";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatCardModule} from "@angular/material/card";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatMenuModule} from "@angular/material/menu";
import {MatToolbarModule} from "@angular/material/toolbar";
import {ReactiveFormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MedicationFormComponent} from './medication-form/medication-form.component';


const routes: Route[] = [
    {path: '', component: MedicationsComponent}
]

@NgModule({
    declarations: [
        MedicationsComponent,
        MedicationFormComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),

        MatSidenavModule,
        MatDividerModule,
        FlexLayoutModule,
        MatCardModule,
        MatPaginatorModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatMenuModule,
        MatToolbarModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatTooltipModule,
        FuseSharedModule,
        MatDatepickerModule,
        MatSelectModule
    ]
})
export class MedicationsModule {
}
