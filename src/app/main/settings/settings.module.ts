import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';

import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatMenuModule} from "@angular/material/menu";
import {MatToolbarModule} from "@angular/material/toolbar";
import {ReactiveFormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FuseSharedModule} from "../../../@fuse/shared.module";
import {UserFormComponent} from './users/user-form/user-form.component';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatSelectModule} from "@angular/material/select";
import {UsersComponent} from './users/users.component';
import {InfosComponent} from './infos/infos.component';
import {MatListModule} from "@angular/material/list";
import {FuseWidgetModule} from "../../../@fuse/components";
import {InfosFormComponent} from './infos/infos-form/infos-form.component';
import {LogoFormComponent} from './infos/logo-form/logo-form.component';
import {FileUploadModule} from 'ng2-file-upload';
import {DatabaseManagementComponent} from './database-management/database-management.component';
import {ImportDatabaseDialogComponent} from './database-management/import-database-dialog/import-database-dialog.component';
import {MedicationsModule} from "./medications/medications.module";
import { PasswordUserFormComponent } from './users/password-user-form/password-user-form.component';

const routes: Routes = [
    {
        path: 'settings',
        children: [
            {path: 'users', component: UsersComponent},
            {path: 'infos', component: InfosComponent},
            {
                path: 'medications',
                loadChildren: () => import('./medications/medications.module').then(m => m.MedicationsModule)
            },
            {path: 'databaseManagement', component: DatabaseManagementComponent},
            {path: '', redirectTo: '/users', pathMatch: 'full'},
        ],
        //  canActivate: [AuthGuard]

    }
]

@NgModule({
    declarations: [

        UserFormComponent,
        UsersComponent,
        InfosComponent,
        InfosFormComponent,
        LogoFormComponent,
        DatabaseManagementComponent,
        ImportDatabaseDialogComponent,
        PasswordUserFormComponent,
    ],
    imports: [
        CommonModule,


        RouterModule.forChild(routes),
        MatPaginatorModule,
        // MatTableDataSource,
        MatSidenavModule,
        MatDividerModule,
        FlexLayoutModule,
        MatCardModule,
        MatPaginatorModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,

        MatMenuModule,
        MatToolbarModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatTooltipModule,
        FuseSharedModule,
        MatDatepickerModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatListModule,
        FuseWidgetModule,

        FileUploadModule,
        /* My Modules */
        MedicationsModule
    ]
})
export class SettingsModule {
}
