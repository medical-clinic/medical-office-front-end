import {Component, Inject, OnInit} from '@angular/core';
import {DatabaseManagementService} from "./database-management.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ImportDatabaseDialogComponent} from "./import-database-dialog/import-database-dialog.component";
import {LogoFormComponent} from "../infos/logo-form/logo-form.component";
import {FuseConfirmDialogComponent} from "../../../../@fuse/components/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'app-database-management',
  templateUrl: './database-management.component.html',
  styleUrls: ['./database-management.component.scss']
})
export class DatabaseManagementComponent implements OnInit {

  constructor(private dbService:DatabaseManagementService,
              public dialog: MatDialog,private _matDialog: MatDialog, ) { }
    dialogRef: any;
  ngOnInit(): void {
  }

    getBackup() {
        this.dbService.getBackup()
            .subscribe(
                res=>{this.downloadFile(res)},
                error => {}
            )
    }

    private downloadFile(data) {
        const blob = new Blob([data], {
            type: 'application/zip'
        });
        const url = window.URL.createObjectURL(blob);
        window.open(url);
    }
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    showConfirmDialogDatabase() {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'We will destroy all data! Are you sure you to import new database?';


        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.importDatabase();
            }
            this.confirmDialogRef = null;
        });
    }
    importDatabase() {


        this.dialogRef = this._matDialog.open(ImportDatabaseDialogComponent, {
            panelClass: 'logo-form-dialog', // todo
            data: {
                action: 'edit',
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {

                if (response.uploadSucceeded) {
                    // todo
                }


                /* if (!response) {
                     return;
                 }
                 const actionType: string = response[0];
                 const formData: FormGroup = response[1];
                 if(actionType=='save') {
                     console.log("'save'")
                         console.log(formData.getRawValue().logo)
                         let file = formData.getRawValue().logo;

                         this.infosService.updateLogo(file, this.infos.id).subscribe(
                             event => {
                                 //console.log(event);
                                this.getInfos()
                             },
                             err => {
                                 console.log(err)

                             });

                 }*/
            });  }
}
