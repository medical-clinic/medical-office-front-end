import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FileUploader} from "ng2-file-upload";
import {API_URL} from "../../../../_shared/config/api.url.config";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {AuthenticationService} from "../../../authentication/login/authentication.service";

@Component({
  selector: 'app-import-database-dialog',
  templateUrl: './import-database-dialog.component.html',
  styleUrls: ['./import-database-dialog.component.scss']
})
export class ImportDatabaseDialogComponent implements OnInit {

    uploader: FileUploader;
    hasBaseDropZoneOver: boolean;
    hasAnotherDropZoneOver: boolean;

    @ViewChild('fileInput') fileInput: ElementRef;
    isDropOver: boolean;

    constructor(public matDialogRef: MatDialogRef<ImportDatabaseDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public dialogData: any,
                private authService: AuthenticationService,
    ) {
        this.uploader = new FileUploader({
            url: `${API_URL.api_database_management}`,
            method: 'PUT',
            itemAlias: 'backup',
            authTokenHeader: 'Authorization',
            authToken: this.authService.accessToken//getToken(),
        });


        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;

        // this.response = '';

        this.uploader.response.subscribe(res => {
                console.log(res)
            },
            (err)=>{
                console.log(err)
            }
        );

        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

    }

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
        this.isDropOver = e;
    }

    ngOnInit(): void {
    }

    fileClicked() {
        this.fileInput.nativeElement.click();
    }

    OnClose() {
        const uploadSucceeded: boolean = this.uploader.queue.length > 0;
        this.matDialogRef.close({uploadSucceeded: uploadSucceeded});
    }



    // dialogTitle: string;
    // action: string;

    // logoForm: FormGroup;
    // constructor(public matDialogRef: MatDialogRef<LogoFormComponent>,
    //             @Inject(MAT_DIALOG_DATA) private _data: any,
    //             private _formBuilder: FormBuilder) {
    //     // Set the defaults
    //     this.action = _data.action;

    //     if (this.action === 'edit') {
    //         this.dialogTitle = 'Edit Logo';


    //     }
    //     // // Set the private defaults
    //     // this._unsubscribeAll = new Subject();

    //     this.logoForm = this.createForm()
    // }
    // /**
    //  * Create user form
    //  *
    //  * @returns {FormGroup}
    //  */
    // createForm(): FormGroup {
    //     return this._formBuilder.group({
    //         logo: new FormControl('',[Validators.required]),
    //     });

    // }
    // ngOnInit(): void {
    // }

    // onFileChange($event) {
    //     this.logoForm.patchValue(
    //         {
    //             logo:<File>$event.target.files[0]
    //         }
    //     )
    // }

}
