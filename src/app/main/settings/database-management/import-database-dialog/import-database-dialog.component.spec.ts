import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportDatabaseDialogComponent } from './import-database-dialog.component';

describe('ImportDatabaseDialogComponent', () => {
  let component: ImportDatabaseDialogComponent;
  let fixture: ComponentFixture<ImportDatabaseDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportDatabaseDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportDatabaseDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
