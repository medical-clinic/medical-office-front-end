import {Component, OnInit} from '@angular/core';
import {InfosService} from "./infos.service";
import {InfosDto} from "./infos.model";
import {FormGroup} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {InfosFormComponent} from "./infos-form/infos-form.component";
import {LogoFormComponent} from "./logo-form/logo-form.component";
import {HttpClient} from "@angular/common/http";

@Component({
    selector: 'app-infos',
    templateUrl: './infos.component.html',
    styleUrls: ['./infos.component.scss']
})
export class InfosComponent implements OnInit {
    public infos: InfosDto = new InfosDto();

    dialogRef: any;
    constructor(private infosService: InfosService,
                private _matDialog: MatDialog, private http: HttpClient) { }

    ngOnInit(): void {
        this.getInfos();
    }
    getInfos() {
        this.infosService.getInfos()
            .subscribe(
                res => {
                    this.infos = res;
                },
                error => console.error(error)
            )
    }

    editInfos(element: InfosDto) {
        this.dialogRef = this._matDialog.open(InfosFormComponent, {
            panelClass: 'infos-form-dialog',
            data: {
                infos: element,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                if (actionType=='save') {
                        this.infosService.updateInfos(formData.getRawValue())
                            .subscribe((res) => {
                                console.log(res);
                                 this.getInfos()
                            }, (err) => console.error(err)
                            );
                }
            });
    }

    editLogo() {
        this.dialogRef = this._matDialog.open(LogoFormComponent, {
            panelClass: 'logo-form-dialog',
            data: {
                action: 'edit',
                logoId: this.infos.id
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {

                if (response.uploadSucceeded) {
                    this.getInfos()
                }


               /* if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                if(actionType=='save') {
                    console.log("'save'")
                        console.log(formData.getRawValue().logo)
                        let file = formData.getRawValue().logo;

                        this.infosService.updateLogo(file, this.infos.id).subscribe(
                            event => {
                                //console.log(event);
                               this.getInfos()
                            },
                            err => {
                                console.log(err)

                            });

                }*/
            });
    }





}
