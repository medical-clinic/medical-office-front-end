import { Injectable } from '@angular/core';
import {API_URL} from "../../_shared/config/api.url.config";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {PageDto} from "../../_shared/_models/page.model";
import {PatientDto} from "../patients/patient.model";
import {ResponseAllPayments} from "./responseAllPayments.model";

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

    private urlResource:string=API_URL.api_payments;
    constructor(private http:HttpClient) { }

    getPayments(pageIndex: number,pageSize: number,
                keyword?: string
    ):Observable<PageDto<ResponseAllPayments>>{

        return this.http.get<PageDto<ResponseAllPayments>>(this.urlResource,
            {
                params: new HttpParams()
                    .set('page',pageIndex.toString())
                    .set('limit',pageSize.toString())
                    //.set('keyword',keyword)

            })
    }
}
