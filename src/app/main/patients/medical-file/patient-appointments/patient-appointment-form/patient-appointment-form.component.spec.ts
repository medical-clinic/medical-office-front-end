import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AppointmentPersonalFormComponent} from './patient-appointment-form.component';

describe('AppointmentPersonalFormComponent', () => {
  let component: AppointmentPersonalFormComponent;
  let fixture: ComponentFixture<AppointmentPersonalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentPersonalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentPersonalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
