
export class PatientAppointmentDto {
    id:number;
    date: string
    time: string
    status: string
    /**
     * Constructor
     *
     * @param appointment
     */
    constructor(appointment) {
        {
           // this.id = appointment.id || FuseUtils.generateGUID();
            this.date = appointment.date|| '';
            this.time = appointment.time|| '';
            this.status= appointment.status|| 'Pending'
        }
    }
}
