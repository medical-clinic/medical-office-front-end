import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {FuseConfirmDialogComponent} from "../../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {AuthenticationService} from "../../../authentication/login/authentication.service";
import {FormGroup} from "@angular/forms";
import {ConsultationsService} from "./consultations.service";
import {ConsultationDto} from "./consultation.model";
import {ConsultationFormComponent} from "./consultation-form/consultation-form.component";
import {convertDate, momentToDate} from '../../../../_shared/helpers'

@Component({
    selector: 'app-consultations',
    templateUrl: './consultations.component.html',
    styleUrls: ['./consultations.component.scss']
})
export class ConsultationsComponent implements OnInit {

    @Input() patientId: number;
    @Input() fullName: string;

    private consultations_list: Array<ConsultationDto> = new Array<ConsultationDto>();
    dataSource = new MatTableDataSource<ConsultationDto>(this.consultations_list);
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;


    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;
    pageEvent: PageEvent;
    pageIndex: number;
    pageSize: number;
    length: number;
    date: string=null;


    constructor(private router: Router,
        private consultationsService: ConsultationsService,
        public dialog: MatDialog,
        private _matDialog: MatDialog
        , public authService: AuthenticationService) {

    }

    ngOnInit(): void {
        this.getServerData(null);
       
    }


    public getServerData(event?: PageEvent) {
        this.pageEvent = event;
        let pi = event != null ? event.pageIndex : 0;
        let ps = event != null ? event.pageSize : 10;
        this.consultationsService.getConsultations(this.patientId, pi, ps, this.date).subscribe(

            response => {
                console.log(response);

                // if (response.error) {
                //     // handle error
                // } else {
                this.consultations_list = new Array<ConsultationDto>()

                response.content.forEach(element => this.consultations_list.push(element));
                this.dataSource.data = this.consultations_list


                this.dataSource._updateChangeSubscription();
                this.pageIndex = response.number;

                this.length = response.totalElements;


                this.pageSize = response.size;
                // }
            },
            error => {
                // handle error
            }
        );
        return event;
    }
    search(date) {
        this.date= convertDate(date)
        this.getServerData(null)
    
    }

    addConsultation() {
       
        this.dialogRef = this._matDialog.open(ConsultationFormComponent, {
            panelClass: 'consultation-form-dialog',
            data: {
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }


                let consultation = response.getRawValue();
                consultation.date = convertDate(consultation.date)
                console.log(consultation)


                this.consultationsService.addConsultation(this.patientId, consultation)
                    .subscribe(
                        response => {
                            console.log(response)
                            this.getServerData(this.pageEvent)
                        },
                        err => console.error(err)
                    );
            });
    }

    /**
     * Delete Patient
     */
    deleteConsultation(id: number) {
       
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log('id', id);


                this.consultationsService.deleteConsultation(id)
                    .subscribe(result => {
                        console.log(result);
                        this.getServerData(this.pageEvent)
                    }, err => {
                        console.error(err);
                    }
                    );
            }
            this.confirmDialogRef = null;
        });

    }

    editConsultation(element: ConsultationDto) {

      
        this.dialogRef = this._matDialog.open(ConsultationFormComponent, {
            panelClass: 'consultation-form-dialog',
            data: {
                consultation: element,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        let rawValue=formData.getRawValue();
                        rawValue.date=convertDate(rawValue.date);
                        this.consultationsService.updateConsultation(this.patientId,rawValue)
                            .subscribe((res) => {
                                console.log(res);
                                this.getServerData(this.pageEvent)
                            }, (err) => console.error(err)
                            );

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteConsultation(formData.getRawValue().id);

                        break;
                }
            });
    }

    onDetailConsultation(consId: number) {
        this.router.navigate(['/consultationDetail', consId, this.patientId, this.fullName])

    }

    public getDisplayedColumns(): string[] {
        return  [ 'reason', 'date', 'total','amountPaid','paymentStatus','Actions'];
    }
}
