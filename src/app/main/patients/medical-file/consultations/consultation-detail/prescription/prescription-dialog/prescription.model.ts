export class PrescriptionDto {
    id: number;
    date: string;
    // medicamentMap: Map<string,string>
    detail:string
    /**
     * Constructor
     *
     * @param prescriptionDto
     */
    constructor(prescriptionDto) {
        {
            // this.id = appointment.id || FuseUtils.generateGUID();
            this.date = prescriptionDto.date || '';
          //  this.medicamentMap= prescriptionDto.medicamentMap || new Map<string,string>();
            this.detail= prescriptionDto.detail || '';


        }
    }
}
