import {Injectable} from '@angular/core';
import {API_URL} from "../../../../../../_shared/config/api.url.config";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PrescriptionDto} from "./prescription-dialog/prescription.model";
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {InfosService} from "../../../../../settings/infos/infos.service";
import {InfosDto} from "../../../../../settings/infos/infos.model";

pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Injectable({
  providedIn: 'root'
})
export class PrescriptionService {

    private urlResource:string=API_URL.api_prescriptions;
    constructor(private http:HttpClient,private infosService:InfosService) { }

    get(consultationId:number) :Observable<PrescriptionDto>{
        return this.http.get<PrescriptionDto>(this.urlResource+`/${consultationId}`);
    }
    isAlreadyExist(consultationId:number) :Observable<Boolean>{
        return this.http.get<Boolean>(this.urlResource+`/isAlreadyExist/${consultationId}`);
    }

    delete(id:number) :Observable<any>{
        return this.http.delete<any>(this.urlResource+`/${id}`);
    }
    save(consultationId:number,prescriptionDto:PrescriptionDto) {
        return  this.http.post(`${this.urlResource}/${consultationId}`,prescriptionDto)
    }
    // updateConsultation(patientId:number,consultation:ConsultationDto) {
    //     return  this.http.put(this.urlResource+`/${patientId}`,consultation)
    // }

    print(detail:string,date:string,action='open' ){
        let dataPrinted:DataPrinted;

        this.infosService.getInfos().subscribe(
            res=>{

                dataPrinted={detail:detail,date:date,infos:res};

                    this.generatePdf(dataPrinted,action)

            },err=>console.error(err)
        )

    }


   //private generatePdf(action = 'open') {
   private generatePdf(dataPrinted:DataPrinted,action = 'open') {

        const documentDefinition = this.getDocumentDefinition(dataPrinted);
        switch (action) {
            case 'open': pdfMake.createPdf(documentDefinition).open(); break;
            case 'print': pdfMake.createPdf(documentDefinition).print(); break;
            case 'download': pdfMake.createPdf(documentDefinition).download(); break;
            default: pdfMake.createPdf(documentDefinition).open(); break;
        }
    }

   private getDocumentDefinition(dataPrinted:DataPrinted) {

        return {
            pageSize: 'A5',
            content: [
                {
                    text: 'Prescription',
                    bold: true,
                    fontSize: 20,
                    alignment: 'center',
                    margin: [0, 0, 0, 20]
                },
                {
                    columns: [
                        [{
                            text: dataPrinted.infos.doctorDto.name,
                            style: 'name'
                        },
                            {
                                text: dataPrinted.infos.doctorDto.specialty
                            },
                            {
                                text: dataPrinted.infos.medicalOfficeName
                            },
                            {
                                text: dataPrinted.infos.address
                            },

                            {
                                text: 'Contant No : ' + dataPrinted.infos.phone,
                            }
                        ],
                        {
                            image: dataPrinted.infos.logoBase64,
                            width: 75,
                            alignment: 'right'
                        }
                    ]
                },

                {
                    text: 'Details',
                    style: 'header',
                },
                {
                    text: dataPrinted.detail
                },
                {
                    text: 'Signature',
                    style: 'sign'
                },
                {
                    columns: [
                        {
                            qr: dataPrinted.infos.doctorDto.name + ', Contact No : ' + dataPrinted.infos.phone + ' ,date: '+dataPrinted.date,
                            fit: 100
                        },
                        {
                            text: dataPrinted.date,
                            alignment: 'right',
                        },
                        {
                            text: `(${dataPrinted.infos.doctorDto.name})`,
                            alignment: 'right',
                        }
                    ]
                }
            ],

            info: {
                title: 'Prescription' + '_Prescription',
                author: dataPrinted.infos.doctorDto.name,
                subject: 'Prescription',
                keywords: 'Prescription, medicament',
            },
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 20, 0, 10],
                    decoration: 'underline'
                }
                ,
                name: {
                    fontSize: 16,
                    bold: true
                },

                sign: {
                    margin: [0, 50, 0, 10],
                    alignment: 'right',
                    italics: true
                },
                date:{
                    alignment: 'right'
                }
            }
        };
    }
}
class DataPrinted{
    detail:string;
    date:string;
    infos:InfosDto;

}
