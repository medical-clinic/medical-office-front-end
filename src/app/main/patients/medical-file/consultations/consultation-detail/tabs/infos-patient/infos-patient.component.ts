import {Component, Input, OnInit} from '@angular/core';
import {PatientDto} from 'app/main/patients/patient.model';
import {PatientsService} from 'app/main/patients/patients.service';


// export interface PatientInfo {
//     fullName: string;
//     date_birth: string;
// }

@Component({
    selector: 'app-infos-patient',
    templateUrl: './infos-patient.component.html',
    styleUrls: ['./infos-patient.component.scss']
})
export class InfosPatientComponent implements OnInit {


    @Input() patientId:number;

public patient:PatientDto
    constructor(private patientService:PatientsService) {
    }
    
    ngOnInit(): void {
        this.patientService.getOnePatient(this.patientId)
      .subscribe(
        patient=>{
          this.patient=patient;
       // console.error(patient.profession )
          
        },err=> console.error(err)
      )
        
    }
    
   
    



}
