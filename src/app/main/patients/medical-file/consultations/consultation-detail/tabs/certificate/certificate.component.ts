import {Component, OnInit} from '@angular/core';
import {Certificate} from "./certaficate.model";
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {InfosService} from "../../../../../../settings/infos/infos.service";

pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.scss']
})
export class CertificateComponent implements OnInit {
    certificate:Certificate;
    constructor(private infosService:InfosService) {
        this.certificate =new Certificate();

    }

    ngOnInit(): void {
        this.infosService.getInfos().subscribe(
            res=>{

                this.certificate.infos=res;
                this.certificate.details=`I, the undersigned Dr ${this.certificate.infos.doctorDto.name}, Doctor of ${this.certificate.infos.doctorDto.specialty},
Certify that the examination of Mr/Ms ${this.certificate.patientFullname},
reveals no contraindications for participating in running competitions. `

            },err=>console.error(err)
        )
    }
    generatePdf(action = 'open') {

        const documentDefinition = this.getDocumentDefinition();
        switch (action) {
            case 'open': pdfMake.createPdf(documentDefinition).open(); break;
            case 'print': pdfMake.createPdf(documentDefinition).print(); break;
            case 'download': pdfMake.createPdf(documentDefinition).download(); break;
            default: pdfMake.createPdf(documentDefinition).open(); break;
        }
    }

    getDocumentDefinition() {

        return {
            pageSize: 'A5',
            content: [
                {
                    text: 'MEDICAL CERTIFICATE',
                    bold: true,
                    fontSize: 20,
                    alignment: 'center',
                    margin: [0, 0, 0, 20]
                },
                {
                    columns: [
                        [{
                            text: this.certificate.infos.doctorDto.name,
                            style: 'name'
                        },
                            {
                                text: this.certificate.infos.doctorDto.specialty
                            },
                            {
                                text: this.certificate.infos.medicalOfficeName
                            },
                            {
                                text: this.certificate.infos.address
                            },
                            {
                                text: 'Contant No : ' + this.certificate.infos.phone,
                            }
                        ],
                        {
                            image: this.certificate.infos.logoBase64,
                            width: 75,
                            alignment: 'right'
                        }
                    ]
                },
                {
                    text: this.certificate.details,
                    style: 'details',
                },
                {
                    text: 'Signature',
                    style: 'sign'
                },
                {
                    columns: [
                        {
                            qr: this.certificate.infos.doctorDto.name + ', Contact No : ' + this.certificate.infos.phone + ' ,date: '+this.certificate.date,
                            fit: 100
                        },
                        {
                            text: this.certificate.date,
                            alignment: 'right',
                        },
                        {
                            text: `(${this.certificate.infos.doctorDto.name})`,
                            alignment: 'right',
                        }
                    ]
                }
            ],

            info: {
                title: 'Certificate' + '_Certificate',
                author: this.certificate.infos.doctorDto.name,
                subject: 'Certificate',
                keywords: 'Certificate',
            },
            styles: {
                details: {
                    margin: [0, 20, 0, 10],

                }
                ,
                name: {
                    fontSize: 16,
                    bold: true
                },

                sign: {
                    margin: [0, 50, 0, 10],
                    alignment: 'right',
                    italics: true
                },
                date:{
                    alignment: 'right'
                }
            }
        };
    }

}
