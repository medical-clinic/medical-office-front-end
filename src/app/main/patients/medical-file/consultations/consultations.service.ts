import {Injectable} from '@angular/core';
import {API_URL} from "app/_shared/config/api.url.config";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ConsultationDto} from "./consultation.model";
import {PageDto} from 'app/_shared/_models/page.model';

@Injectable({
  providedIn: 'root'
})
export class ConsultationsService {



    // private host:string='http://localhost:8084';
    private urlResource:string=API_URL.api_consultations;
    constructor(private http:HttpClient) { }

    getConsultations(idPatient:number, pageIndex: number,pageSize: number,
        date: string=null) :Observable<PageDto<ConsultationDto>>{
            if(date=='Invalid date'){
                date=null
            }
            const form = new FormData;
            form.append('idPatient',idPatient.toString());
            form.append('page',pageIndex.toString());
            form.append('limit',pageSize.toString());
            form.append('date',date);

        return this.http.post<PageDto<ConsultationDto>>(this.urlResource,form)

    }
    deleteConsultation(id:number) :Observable<ConsultationDto>{
        return this.http.delete<ConsultationDto>(this.urlResource+`/${id}`);
    }
    addConsultation(patientId:number,consultation:ConsultationDto) {
       
        return  this.http.post(`${this.urlResource}/${patientId}`,consultation)
    }
    updateConsultation(patientId:number,consultation:ConsultationDto) {
        return  this.http.put(this.urlResource+`/${patientId}`,consultation)
    }


}
