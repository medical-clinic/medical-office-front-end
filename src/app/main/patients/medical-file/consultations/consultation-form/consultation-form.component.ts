import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ConsultationDto} from "../consultation.model";
import {stringToDate} from "../../../../../_shared/helpers";

@Component({
    selector: 'app-consultation-form',
    templateUrl: './consultation-form.component.html',
    styleUrls: ['./consultation-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ConsultationFormComponent implements OnInit {

    action: string;
    consultation: ConsultationDto;
    consultationForm: FormGroup;
    dialogTitle: string;
    totalCost: number = 0;

    /**
     * Constructor
     *
     * @param {MatDialogRef<AppointmentFormComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ConsultationFormComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Consultation';
            this.consultation = _data.consultation;
            this.totalCost=this.consultation.payment.total;
        } else {
            this.dialogTitle = 'New Consultation';
            this.consultation = new ConsultationDto({});
        }

        this.consultationForm = this.createConsultationForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createConsultationForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.consultation.id],
            reason: [this.consultation.reason],
            payment: this._formBuilder.group(
                {
                    total: [this.consultation.payment.total, [Validators.min(0)]],
                    amountPaid: [this.consultation.payment.amountPaid,
                        [Validators.min(0),
                            (control: AbstractControl) => Validators.max(this.totalCost)(control)//ValidateOrigin
                        ]],
                }
            ),
            date: [stringToDate(this.consultation.date)],

        });


    }

    ngOnInit(): void {
    }

}

// export function ValidateOrigin(control: AbstractControl): {[key: string]: boolean} | null {
//     if (control.value < this.scalesForm.controls.scales[3].controls.min.value && control.value > this.scalesForm.controls.scales[3].controls.max.value) {
//         return { invalidOrigin: true };
//     } else {
//         return null;
//     }
// }